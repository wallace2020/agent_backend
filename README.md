## 项目初始化 - 请先安装代理 AGENT 项目

~~~~初始化请遵循以下步骤

- 初始化项目第三方扩展库 composer update

- 初始化数据库 php artisan migrate

- 复制 .env.example => .env 文件

- 配置数据库
  
  DB_CONNECTION=mysql
  DB_HOST=127.0.0.1
  DB_PORT=3306
  DB_DATABASE=数据库名
  DB_USERNAME=用户
  DB_PASSWORD=密码
  
- 执行默认数据库文件
  /public/install.sql
  
  
- 配置Redis
  REDIS_HOST=127.0.0.1
  REDIS_PASSWORD=null
  REDIS_PORT=6379

- 增加权限
    - 项目权限 
    - chown -R www:www storage
    - chmod -R 775 storage

- 使用git忽略掉权限变化
    - 修改 .git/config => filemode = false

- 配置nginx
    server {
         listen       80;
         server_name  xxx.xxx.com;
         root   "/xxx/public";
         
         index index.php index.html;
         
         location / {
            try_files $uri $uri/ /index.php?$query_string;
         }
         
         location ~ \.php(.*)$ {
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            fastcgi_split_path_info  ^((?U).+\.php)(/?.+)$;
            fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
            fastcgi_param  PATH_INFO  $fastcgi_path_info;
            fastcgi_param  PATH_TRANSLATED  $document_root$fastcgi_path_info;
            include        fastcgi_params;
         }
    }
    
- 访问网站后台
  域名/admin
  
- 如果发现IP异常
  1、进入数据库
  2、找到 ip_whilte 表 
     加入自己的IP，
     访问时间设置为 -1，
     adminId设置为1，
     选择添加和修改时间

- 添加完毕后
  访问 ： 域名/clearCache?pass=wallace, 然后重新的登录

- 定时任务
  添加 crontab -e -u www
  * * * * * /usr/local/bin/php /agent/artisan schedule:run
