<?php


namespace App\Admin\Controllers\PlugIn\FormBtn;

use App\Admin\Controllers\UserTools\CommissionStatisticsController;
use Encore\Admin\Actions\BatchAction;
use Illuminate\Database\Eloquent\Collection;
use App\Admin\Controllers\UserTools\HandlesController;

class BatchStatus extends BatchAction
{

    public $name = '批量审批';

    public function handle(Collection $collection)
    {
        $handles = new HandlesController();
        $success = $fail = 0;
        foreach ($collection as $val) {
            $result = $handles->_audit($val->id);
            if ($result == 0) {
                ++$success;
            } else {
                ++$fail;
            }
        }
        return $this->response()->success("处理成功 : {$success} ,失败 : {$fail}")->refresh();
    }

    public function form()
    {
        $this->radio('status', __('方案状态'))->options([
            -1 => '审批不通过',
            1 => '审批通过',
        ])->default(0);
    }

}
