<?php

namespace App\Admin\Controllers\PlugIn\FormBtn;

use Encore\Admin\Facades\Admin;

class IpRedis
{

    protected $route;

    public $className;

    protected $calledClass;

    public function __construct()
    {
        $this->route = route('ipManage.refresh');
        $this->className = time() . '-ipredis-' .rand(10000, 99999);
        $this->calledClass = str_replace('\\', '_', get_called_class());
    }

    public function render()
    {
        Admin::script($this->script());
        return "<a class='report-posts btn btn-sm btn-warning {$this->className}'><i class='fa fa-info-circle'></i>刷新配置</a>";
    }

    public function __toString()
    {
        return $this->render();
    }

    protected function script()
    {
        $script = <<<SCRIPT
$('.$this->className').unbind('click').click(function() {
    swal({
        title: "IP刷新",
        type: "info",
        showCancelButton: true,
        confirmButtonText: "确认",
        showLoaderOnConfirm: true,
        cancelButtonText: "取消",
        preConfirm: function(input) {
            return new Promise(function(resolve, reject) {
                var data = {}
                Object.assign(data, {
                    _token: $.admin.token,
                    _action: '$this->calledClass',
                    _input: input
                });

                $.ajax({
                    method: 'post',
                    url: '$this->route',
                    data: data,
                    success: function (data) {
                        $.pjax.reload('#pjax-container');
                        if(data.code == 0){
                            swal(data.message, '', 'success');
                        }else{
                            swal(data.message, '', 'error');
                        }
                    },
                    error:function(request){
                        reject(request);
                    }
                });
            });
        }
    });
});
SCRIPT;
        return $script;
    }

}
