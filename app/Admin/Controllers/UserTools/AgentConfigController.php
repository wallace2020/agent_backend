<?php


namespace App\Admin\Controllers\UserTools;

use App\Models\AgentConfig;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;

class AgentConfigController extends BaseController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '网站设置';

    /**
     * 部门首页
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * 修改部门信息
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('修改')
            ->body($this->form()->edit($id));
    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AgentConfig);

        $grid->fixColumns(5, -2);
        $grid->column('id', __('ID'))->sortable();
        $grid->column('defaultUrl', __('代理默认URL'));
        $grid->column('created_at', __('创建时间'));

        $grid->disableActions();

        return $grid;
    }

    /**
     * 表单详情
     * @return Form
     */
    protected function form()
    {
        //TODO 检查权限
        $form = new Form(new AgentConfig);

        $form->text('defaultUrl', __('默认推广链接'));

        $form->saved(function (Form $form) {
            return redirect(route('config.index'));
        });

        return $form;
    }

    public function detail() {
        return redirect(route('config.index'));
    }

}
