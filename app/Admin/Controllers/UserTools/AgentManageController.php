<?php


namespace App\Admin\Controllers\UserTools;

use App\Models\Agent;
use App\Models\AgentBank;
use App\Models\Bank;
use App\Models\Commission;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Zhusaidong\GridExporter\Exporter;
use Encore\Admin\Controllers\AdminController;

class AgentManageController extends AdminController
{

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '代理列表';

    const _status = [
        -1 => '禁用',
         0 => '审核中',
         1 => '启用',
    ];

    const status = [
        -1 => '<span class="label label-danger">禁用</span>',
         0 => '<span class="label label-info">审核中</span>',
         1 => '<span class="label label-success">启用</span>',
    ];

    const _valid = [
        0 => '未验证',
        1 => '审核中',
        2 => '已验证',
    ];

    const valid = [
        0 => '<span class="label label-danger">未验证</span>',
        1 => '<span class="label label-info">审核中</span>',
        2 => '<span class="label label-success">已验证</span>',
    ];

    /**
     * 代理首页
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * 创建代理信息
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('创建')
            ->body($this->form(0));
    }

    /**
     * 修改代理信息
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('修改')
            ->body($this->form($id)->edit($id));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Agent);

        $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->like('agentNo', __('代理编号'));
            $filter->like('agentName', __('代理账号'));
            $_statusOptions = ['' => '全部'] + self::_status;
            $filter->where(function ($query) {
                if ($this->input != '') {
                    $query->where('status', $this->input);
                }
            }, __('代理状态'), 'status')->radio($_statusOptions);
            $filter->expand();
        });

        $grid->column('id', __('ID'))->sortable();
        $grid->column('commissionId', __('佣金方案'))->display(function($commissionId) {
            $name = '';
            $commission = Commission::where('id', $commissionId)->select('commissionName')->first();
            if ($commission) {
                $name = $commission->commissionName;
            }
            return $name;
        });
        $grid->column('agentNo', __('代理编号'));
        $grid->column('agentName', __('代理账号'));
        $grid->column('status', __('代理状态'))->using(self::status);
        $grid->column('created_at', __('创建时间'));

        //设置导出格式
        $exporter = Exporter::get($grid);
        $grid->exporter($exporter);

        $grid->actions(function ($actions) {
            //关闭行操作 删除
            $actions->disableView();
            if (!Admin::user()->isRole('administrator')) {
                $actions->disableEdit();
                $actions->disableDelete();
            }
        });

        if (!Admin::user()->isRole('administrator')) {
            $grid->disableRowSelector();
        }

        $grid->model()->orderBy('created_at', 'desc');

        return $grid;
    }

    /**
     * 表单详情
     * @return Form
     */
    protected function form($id = 0)
    {
        //TODO 检查权限
        $form = new Form(new Agent);

        $inviteCode = '';
        if (intval($id) <= 0 && !$_POST) {
            $inviteCode = $this->_createInviteCode();
        }

        $realName = $phone = $email = $password = $payword = '';
        if ($id > 0) {
            $info = Agent::select('realName', 'phone', 'email', 'password', 'payword')->where('id', $id)->first();
            if ($info) {
                $realName = $info->realName ? unSecret($info->realName, 'username') : '';
                $phone = $info->phone ? unSecret($info->phone, 'phone') : '';
                $email = $info->email ? unSecret($info->email, 'email') : '';
                $password = $info->password;
                $payword = $info->payword;
            }
        }

        $form->tab('代理信息', function (Form $form) use ($inviteCode) {
            $arr[] = '请选择佣金方案';

            $commissions = Commission::select('commissionName', 'id')->where('commissionStatus', 1)->get();
            if (!$commissions->isEmpty()) {
                foreach ($commissions as $val) {
                    $arr[$val->id] = $val->commissionName;
                }
            }

            $form->select('parent_id', __('上级代理'))
                ->options(Agent::where('status', 1)->pluck('agentName', 'id'))
                ->load('commissionId', '/admin/getCommission')
                ->help('不设置则为一级代理')->setWidth(3);
            $form->text('agentNo', __('代理编号'))->required()->rules('required')->setWidth('3');
            $form->switch('status', __('代理状态'))->default(1);
            $form->text('agentName', __('代理账号'))->setWidth('3')->required()->rules('required');
            $form->text('inviteCode', __('推广码'))->default($inviteCode)->readonly()->setWidth('3')->required()->rules('required');
            $form->select('commissionId', __('佣金方案'))
                ->options($arr)
                ->setWidth('3')
                ->required()
                ->rules('required');
            $form->display('wallet', __('佣金钱包'))->with(function ($value) {
                return amountFilter($value);
            })->setWidth('3');
            $form->hidden('level', __('代理等级'))->default(0)->setWidth('3');
            $form->text('info.generalizeLinkMain', __('主推广域名'))->setWidth('3');
            $form->text('info.generalizeLinkSecond', __('推广域名'))->setWidth('3');
            $form->text('info.generalizeLinkThird', __('推广域名'))->setWidth('3');
            $form->hidden('id');
            $form->hidden('registerSource')->default('pc');
            $form->hidden('registerIp')->default(request()->getClientIp());
        });
        if ($id == 0) {
            $form->hidden('remark', '备注')->default('后台注册');
        }

        $form->tab('基本信息', function (Form $form) use ($realName, $email, $phone, $password, $payword) {
            $form->text('_realName', __('真实姓名'))->default($realName)
                ->required()->rules('required')->setWidth('3');
            $form->radio('realNameValid', __('真实姓名验证状态'))->options(self::_valid)->default(0);
            $form->hidden('realName');

            $form->mobile('_phone', __('电话号码'))->default($phone)
                ->required()->rules('required')->setWidth('3');
            $form->radio('phoneValid', __('电话号码验证状态'))->options(self::_valid)->default(0);
            $form->hidden('phone');

            $form->email('_email', __('邮箱地址'))->default($email)
                ->required()->rules('required')->setWidth('3');
            $form->radio('emailValid', __('邮箱验证状态'))->options(self::_valid)->default(0);
            $form->hidden('email');

            $form->password('_password', __('登录密码'))->default('')->setWidth('3');
            $form->password('_payword', __('支付密码'))->default('')->setWidth('3');
            $form->hidden('password', $password);
            $form->hidden('payword', $payword);
            $form->date('info.birthDay', __('生日'));
            $form->text('info.qq', __('QQ'))->setWidth('3');
            $form->text('info.wechat', __('微信'))->setWidth('3');
        });

        //$bank = BankCard::getBankList();
        $bank = Bank::where([
                'status' => 1,
                'type' => 2
            ])
            ->pluck('bankName', 'id');

        $form->tab('银行卡信息', function (Form $form) use ($bank) {
            $form->hasMany('bank', '银行卡管理', function (Form\NestedForm $form) use ($bank) {
                $form->switch('isMain', __('是否为主卡'))->help('主卡只能有一张');
                $form->select('bankCode', __('银行名称'))->options($bank)
                    ->load('bankName', '/admin/getBankName')
                    ->required()
                    ->setWidth(3);
                $form->select('bankName', __('银行名称'))->setGroupClass('hide')->setWidth(3);
                $form->text('bankAccount', __('开户卡号'))->setWidth(3);
                $form->text('bankUserName', __('开户人'))->setWidth('3');
                $form->text('bankBranchName', __('支行地址'))->setWidth('3');
                $form->switch('status', __('卡片状态'))->default(1);
                //$form->distpicker(['province', 'city', 'area'], '请选择区域')->autoselect(3);
                $form->hidden('bankCount', '卡片总数')->default(0);
                $form->hidden('province')->default(0);
                $form->hidden('city')->default(0);
                $form->hidden('area')->default(0);
            });
        });

        $form->tab('登录信息', function (Form $form) {
            $form->display('lastLoginSource', __('最后登录来源'))->setWidth('3');
            $form->display('lastLoginIp', __('最后登录IP'))->readonly()->setWidth('3');
            $form->display('lastLoginAt', __('最后登录时间'))->readonly()->setWidth('3');
        });

        $form->tools(function (Form\Tools $tools){
            //去掉预览页面
            $tools->disableView();
            if (!Admin::user()->isRole('administrator')) {
                $tools->disableDelete();
            };
        });

        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });

        $form->ignore(['_realName', '_phone', '_email', '_payword', '_password']);

        $form->saving(function (Form $form) use ($bank) {
            $form->realName = request('_realName', '');
            if ($form->realName) {
                $form->realName = doSecret($form->realName, 'username');
            }
            $form->phone = request('_phone', '');
            if ($form->phone) {
                $form->phone = doSecret($form->phone, 'phone');
            }
            $form->email = request('_email', '');
            if ($form->email) {
                $form->email = doSecret($form->email, 'email');
            }

            $_payword = request('_payword', '');
            if ($_payword) {
                $form->payword = doSecret($_payword, 'password');
            }

            $_password = request('_password', '');
            if ($_password) {
                $form->password = doSecret($_password, 'password');
                if (empty($_payword)) {
                    $form->payword = $form->password;
                }
            }

            $form->bankCount = 0;
            if ($form->id > 0) {
                $form->bankCount = AgentBank::where('agentId', $form->id)->count() + 1;
            }
        });

        $form->saved(function(Form $form) use ($bank) {
            $id = $form->model()->id;

            $agent = Agent::where('id', $id)->where('status', 1)->first();

            //设置代理层级
            if ($agent) {
                try {
                    if ($agent->parent_id > 0) {
                        $parent = Agent::where('id', $agent->parent_id)->first();
                        $parent->moveToLeftOf($agent);
                    } else {
                        $node = $agent->makeRoot();
                        $node->parent_id = 0;
                        $node->save();
                    }
                } catch (\Exception $e) {
                    logRecord($e->getMessage());
                }
            }

            return redirect(route('agent.index'));
        });

        return $form;
    }

    /**
     * 获取所有一级菜单
     * @return mixed
     */
    public function getParentOptions()
    {
        $arr[] = [
            'id' => 0,
            'text' => '请选择代理'
        ];

        $options = Agent::select('agentName', 'id')->where('status', 1)->get();
        if (!$options->isEmpty()) {
            foreach ($options as $val) {
                $arr[] = [
                    'id' => $val->id,
                    'text' => $val->agentName
                ];
            }
        }

        return $arr;
    }

    public function getAgent()
    {
        $id = request()->get('q', '');

        if (is_numeric($id) && intval($id) > 0) {
            $options = Agent::select('id', 'agentName')
                ->where('id', $id)
                ->where('status', 1)
                ->get();
            if (!$options->isEmpty()) {
                foreach ($options as $val) {
                    $arr[] = [
                        'id' => $val->id,
                        'text' => $val->agentName
                    ];
                }
            }
        }

        return $arr;
    }

    public function detail() {
        return redirect(route('agent.index'));
    }

    /****************************  私有方法 ***********************/
    //创建邀请码
    private function _createInviteCode() : string
    {
        //TODO 判断代理邀请码是否重复 -- 查主库
        $inviteCode = createOrderNo('A');
        $count = Agent::on('mysql')->where('inviteCode', $inviteCode)->count();
        if ($count <= 0) {
            return $inviteCode;
        }

        //TODO 添加日志
        logRecord("_createInviteCode exists {$inviteCode}");
        $this->_createInviteCode();
    }

}
