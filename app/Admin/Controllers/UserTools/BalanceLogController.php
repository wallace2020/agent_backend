<?php


namespace App\Admin\Controllers\UserTools;

use App\Models\Agent;
use App\Models\BalanceLog;
use App\Models\Bank;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Zhusaidong\GridExporter\Exporter;
use Encore\Admin\Layout\Content;

class BalanceLogController extends BaseController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '代理余额增加记录';

    const _type = [
        1 => '增加',
        2 => '减少',
    ];

    const type = [
        1 => '<span class="label label-success">增加</span>',
        2 => '<span class="label label-default">减少</span>',
    ];

    const _status = [
        -1 => '审批不通过',
         0 => '待审批',
         1 => '已通过'
    ];

    const status = [
        -1 => '<span class="label label-danger">审批不通过</span>',
         0 => '<span class="label label-default">待审批</span>',
         1 => '<span class="label label-success">已通过</span>',
    ];

    /**
     * 代理余额修改首页
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * 代理余额修改信息
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('修改')
            ->body($this->form($id)->edit($id));
    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BalanceLog);

        $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->like('agentName', __('代理名称'));
            $_statusOptions = ['' => '全部'] + self::_status;
            $filter->where(function ($query) {
                if ($this->input != '') {
                    $query->where('status', $this->input);
                }
            }, __('审批状态'), 'status')->radio($_statusOptions);

            $_typeOptions = ['' => '全部'] + self::_type;
            $filter->where(function ($query) {
                if ($this->input != '') {
                    $query->where('type', $this->input);
                }
            }, __('方式'), 'type')->radio($_typeOptions);
            $filter->expand();
        });

        $grid->column('id', __('ID'))->sortable();
        $grid->column('agentName', __('代理名称'));
        $grid->column('money', __('金额'))->display(function(){
            return amountFilter($this->money);
        });
        $grid->column('type', __('方式'))->using(self::type);
        $grid->column('status', __('审核状态'))->using(self::status);
        $grid->column('remark', __('备注'));
        $grid->column('creator_name', __('发起人'));
        $grid->column('approval_name', __('审核人'));
        $grid->column('auditTime', __('审批时间'));
        $grid->column('created_at', __('创建时间'));

        $grid->actions(function ($actions) {
            //关闭行操作 删除
            $actions->disableView();
            if (!Admin::user()->isRole('administrator')) {
                $actions->disableEdit();
                $actions->disableDelete();
            }
        });

        if (!Admin::user()->isRole('administrator')) {
            $grid->disableRowSelector();
        }

        //设置导出格式
        $exporter = Exporter::get($grid);
        $grid->exporter($exporter);

        return $grid;
    }

    /**
     * 表单详情
     * @return Form
     */
    protected function form($id = 0)
    {
        //TODO 检查权限
        $form = new Form(new BalanceLog);

        $form->select('agentId', __('选择代理'))
            ->options('/admin/getParentOptions')
            ->required()
            ->rules('required|numeric|gt:0');

        if ($id > 0) {
            $info = BalanceLog::select('money')->where('id', $id)->first();
            $form->text('_money', __('金额'))->default(amountFilter($info->money));
            $form->hidden('money')->default($info->money);
        } else {
            $form->text('money', __('金额'));
        }

        if ($id == 0) {
            $form->radio('type',  __('方式'))->options(self::_type)->default(0);
            $form->hidden('status')->default(0);
        } else {
            $_type = self::type;
            $form->display('type',  __('方式'))->with(function ($type) use ($_type) {
                return $_type[$type];
            });
            $form->hidden('type')->default($form->type);
        }

        if (Admin::user()->isRole('administrator')) {
            $approval_id = BalanceLog::select('approval_id')->where('id', $id)->first();
            if ($id > 0) {
                if (empty($approval_id->approval_id)) {
                    $form->radio('status', __('审批结果'))->options(self::_status)->default(0);
                } else {
                    $_status = self::status;
                    $form->display('status',  __('审批结果'))->with(function ($status) use ($_status) {
                        return $_status[$status];
                    });
                    $form->disableSubmit();
                    $form->disableReset();
                }
            }
        }

        $form->hidden('id');
        $form->hidden('approval_id');
        $form->hidden('approval_name');
        $form->hidden('auditTime');
        $form->hidden('auditIp');
        $form->hidden('agentName', __('代理姓名'));
        $form->hidden('creator_id')->default(Admin::user()->id);
        $form->hidden('creator_name')->default(Admin::user()->username);
        $form->textarea('remark', __('备注'));

        $form->tools(function (Form\Tools $tools){
            //去掉预览页面
            $tools->disableView();
            if (!Admin::user()->isRole('administrator')) {
                $tools->disableDelete();
            };
        });

        $form->ignore('_money');

        $form->saving(function(Form $form) {
            $id = request()->input('id', '');
            if ($id > 0) {
                $form->money = request()->input('_money', 0);
            }

            if ($form->agentId > 0) {
                $agent = Agent::select('agentName')->where('id', $form->agentId)->first();
                $form->agentName = $agent->agentName ? $agent->agentName : '';
            }
            //减少金额
            if ($form->status == -1) {
                $form->approval_id = Admin::user()->id;
                $form->approval_name = Admin::user()->username;
                $form->auditTime = date('Y-m-d H:i:s');
                $form->auditIp = request()->getClientIp();
                $update = Agent::where('id', $form->agentId)->decrement('wallet', $form->money);
                if (!$update) {
                    return back()->with(admin_toastr('审批失败', 'error'));
                }
            }
            //增加金额
            if ($form->status == 1) {
                $form->approval_id = Admin::user()->id;
                $form->approval_name = Admin::user()->username;
                $form->auditTime = date('Y-m-d H:i:s');
                $form->auditIp = request()->getClientIp();
                $update = Agent::where('id', $form->agentId)->increment('wallet', $form->money);
                if (!$update) {
                    return back()->with(admin_toastr('审批失败', 'error'));
                }
            }
        });

        $form->saved(function(Form $form){
            return redirect(route('balance.index'));
        });

        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });

        return $form;
    }

    public function detail() {
        return redirect(route('balance.index'));
    }

}
