<?php


namespace App\Admin\Controllers\UserTools;

use App\Models\Bank;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Illuminate\Http\Request;
use Zhusaidong\GridExporter\Exporter;
use Encore\Admin\Layout\Content;

class BankController extends BaseController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '银行卡管理';

    const _type = [
        1 => '出款银行',
        2 => '入款银行',
    ];

    const type = [
        1 => '<span class="label label-default">出款银行</span>',
        2 => '<span class="label label-success">入款银行</span>',
    ];

    const _status = [
        0 => '未启用',
        1 => '启用',
    ];

    const status = [
        0 => '<span class="label label-default">未启用</span>',
        1 => '<span class="label label-success">启用</span>',
    ];

    /**
     * 银行卡首页
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * 银行卡信息
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('修改')
            ->body($this->form()->edit($id));
    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Bank);

        $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->like('bankName', __('银行卡名称'));
            $_statusOptions = ['' => '全部'] + self::_status;
            $filter->where(function ($query) {
                if ($this->input != '') {
                    $query->where('status', $this->input);
                }
            }, __('银行卡状态'), 'status')->radio($_statusOptions);

            $_typeOptions = ['' => '全部'] + self::_type;
            $filter->where(function ($query) {
                if ($this->input != '') {
                    $query->where('type', $this->input);
                }
            }, __('银行卡类型'), 'type')->radio($_typeOptions);
            $filter->expand();
        });

        $grid->column('id', __('ID'))->sortable();
        $grid->column('bankName', __('银行名称'));
        $grid->column('bankCode', __('银行编号'));
        $grid->column('bankImg')->image('', 50, 50);
        $grid->column('status', __('银行卡状态'))->using(self::status);
        $grid->column('type', __('银行类型'))->using(self::type);
        $grid->column('created_at', __('创建时间'));

        $grid->actions(function ($actions) {
            //关闭行操作 删除
            $actions->disableView();
            if (!Admin::user()->isRole('administrator')) {
                $actions->disableEdit();
                $actions->disableDelete();
            }
        });

        if (!Admin::user()->isRole('administrator')) {
            $grid->disableRowSelector();
        }

        //设置导出格式
        $exporter = Exporter::get($grid);
        $grid->exporter($exporter);

        return $grid;
    }

    /**
     * 表单详情
     * @return Form
     */
    protected function form()
    {
        //TODO 检查权限
        $form = new Form(new Bank);

        $form->text('bankName', __('银行卡名称'));
        $form->text('bankCode', __('银行卡编号'));
        $form->image('bankImg', __('银行卡图标'));
        $form->switch('status', __('银行卡状态'))->default(1);
        $form->radio('type',  __('银行卡类型'))->options([1 => '出款银行', 2 => '入款银行'])->default(1);

        $form->tools(function (Form\Tools $tools){
            //去掉预览页面
            $tools->disableView();
            if (!Admin::user()->isRole('administrator')) {
                $tools->disableDelete();
            };
        });

        $form->saved(function(){
           return redirect(route('bank.index'));
        });

        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });

        return $form;
    }

    public function detail() {
        return redirect(route('bank.index'));
    }

    public function getBankName(Request $request)
    {
        $data = $request->all();
        $arr = [];
        if (intval($data['q']) > 0) {
            $id = intval($data['q']);
            $bank = Bank::select('id', 'bankName')
                ->where([
                    'id' => $id,
                    'status' => 1
                ])
                ->first();
            if ($bank) {
                $arr[] = [
                    'id' => $bank->id,
                    'text' => $bank->bankName
                ];
            }
        }
        return $arr;
    }

}
