<?php


namespace App\Admin\Controllers\UserTools;

use App\Models\Agent;
use App\Models\AgentLevel;
use App\Models\Commission;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Zhusaidong\GridExporter\Exporter;
use Encore\Admin\Layout\Content;

class CommissionController extends BaseController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '佣金方案';

    const _default = [
        0 => '否',
        1 => '是',
    ];

    const defaults = [
        0 => '<span class="label label-default">否</span>',
        1 => '<span class="label label-success">是</span>',
    ];

    const _status = [
        0 => '未启用',
        1 => '启用',
    ];

    const status = [
        0 => '<span class="label label-default">未启用</span>',
        1 => '<span class="label label-success">启用</span>',
    ];


    /**
     * 部门首页
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * 修改部门信息
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('修改')
            ->body($this->form()->edit($id));
    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Commission);

        $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->like('commissionName', __('代理编号'));
            $_statusOptions = ['' => '全部'] + self::_status;
            $filter->where(function ($query) {
                if ($this->input != '') {
                    $query->where('commissionStatus', $this->input);
                }
            }, __('方案状态'), 'commissionStatus')->radio($_statusOptions);
            $filter->expand();
        });

        $grid->column('id', __('ID'))->sortable();
        $grid->column('isDefault', __('默认方案'))->using(self::defaults);
        $grid->column('commissionName', __('方案名称'));
        $grid->column('commissionStatus', __('方案状态'))->using(self::status);
        $grid->column('created_at', __('创建时间'));

        $grid->actions(function ($actions) {
            //关闭行操作 删除
            $actions->disableView();
            if (!Admin::user()->isRole('administrator')) {
                $actions->disableEdit();
                $actions->disableDelete();
            }
        });

        if (!Admin::user()->isRole('administrator')) {
            $grid->disableRowSelector();
        }

        //设置导出格式
        $exporter = Exporter::get($grid);
        $grid->exporter($exporter);

        return $grid;
    }

    /**
     * 表单详情
     * @return Form
     */
    protected function form()
    {
        //TODO 检查权限
        $form = new Form(new Commission);

        $form->tab('前置信息', function (Form $form) {
            $form->text('commissionName', __('方案名称'))->required()->rules('required')->setWidth(4);
            if (Admin::user()->isRole('administrator')) {
                $form->switch('isDefault', __('默认方案'));
            } else {
                $form->hidden('isDefault', __('默认方案'))->default(0);
            }
            $form->switch('commissionStatus', __('方案状态'))->default(1);
            $form->radio('commissionType',  __('佣金类型'))->options([0 => '占城', 1 => '抽佣'])->default(1);
            $form->radio('commissionTime',  __('计算周期'))->options([0 => '日结', 1 => '周结', 2 => '月结'])->default(0);
            $form->hidden('isCunsum', __('是否公司盈利累加'))->default(0);
            $form->hidden('bindCount', __('绑定数量'))->default(0);
            $form->hidden('adminId', __('操作人'))->default(Admin::user()->id);
            $form->hidden('adminName', __('操作人姓名'))->default(Admin::user()->username);
        });

        $form->tab('方案信息', function (Form $form) {
            $form->number('validMember', __('有效会员数'))->rules('required');
            $form->decimal('validAmount', __('有效投注额'))->rules('required');

            $form->hasMany('level', '佣金层级管理', function (Form\NestedForm $form) {
                $arr[] = '层级编号';
                $commissionLevel = AgentLevel::select('title', 'id')->where('status', 1)->get();
                if (!$commissionLevel->isEmpty()) {
                    foreach ($commissionLevel as $val) {
                        $arr[$val->id] = $val->title;
                    }
                }
                $form->select('commissionLevel', '层级编号')->options($arr)->rules('required')->setWidth(2);
                $form->decimal('minAmount', __('最小金额'))->rules('required');
                $form->decimal('maxAmount', __('最大金额'))->rules('required');
                $form->rate('rate', __('佣金比例'))->rules('required')->setWidth(2);
                $form->hidden('adminId', __('操作人'))->default(Admin::user()->id);
                $form->hidden('adminName', __('操作人姓名'))->default(Admin::user()->username);
            });
        });

        $form->tab('限制信息', function (Form $form) {
            $form->hasMany('config', '参数配置', function (Form\NestedForm $form) {
                $form->select('fieldName', '配置名称')->options([
                    'addAgent' => '添加子代理',
                    'realName' => '真实姓名',
                    'email' => '电子邮件',
                    'phone' => '手机号码',
                    'bank' => '银行账号',
                    'addMember' => '新增会员',
                    'birthday' => '出生日期',
                    'qq' => 'QQ号码',
                    'withdrawBank' => '出款银行',
                    'withdrawWord' => '取款密码',
                ])->rules('required')->setWidth(4);
                $form->radio('type',  __('配置类型'))->options([0 => '状态', 1 => '字段'])->default(0);
                $form->switch('status', __('是否启用'))->help('为状态时必填');
                $form->switch('required', __('是否必填'))->help('为字段时必填');
                $form->hidden('visible', __('是否可见'))->default(1);
                $form->hidden('adminId', __('操作人'))->default(Admin::user()->id);
                $form->hidden('adminName', __('操作人姓名'))->default(Admin::user()->username);
            });
        });

        $form->tools(function (Form\Tools $tools){
            //去掉预览页面
            $tools->disableView();
            if (!Admin::user()->isRole('administrator')) {
                $tools->disableDelete();
            };
        });

        $form->saved(function(){
           return redirect(route('commission.index'));
        });

        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });

        return $form;
    }

    /**
     * 部门详情
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        //TODO 检查权限
        $show = new Show(Commission::findOrFail($id));
        return $show;
    }

    public function getCommission()
    {
        $id = request()->get('q', '');

        $commissionId = 0;
        if ($id != '') {
            $agent = Agent::where('id', $id)->select('commissionId')->first();
            if ($agent) {
                $commissionId = $agent->commissionId;
            }
        }

        $arr = [];

        if($commissionId != 0) {
            $commissions = Commission::select('commissionName', 'id')->where('id', $commissionId)->where('commissionStatus', 1)->get();
        } else {
            $commissions = Commission::select('commissionName', 'id')->where('commissionStatus', 1)->get();
        }

        if (!$commissions->isEmpty()) {
            foreach ($commissions as $val) {
                $arr[] = [
                    'id' => $val->id,
                    'text' => $val->commissionName
                ];
            }
        }

        return $arr;
    }

}
