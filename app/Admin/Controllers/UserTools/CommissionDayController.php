<?php


namespace App\Admin\Controllers\UserTools;

use App\Models\CommissionDay;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Zhusaidong\GridExporter\Exporter;
use Encore\Admin\Layout\Content;

class CommissionDayController extends BaseController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '代理日报表';

    /**
     * 部门首页
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * 修改部门信息
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('修改')
            ->body($this->form()->edit($id));
    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CommissionDay);

        $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->like('agentNo', __('代理编号'));
        });
        $grid->fixColumns(5, -2);
        $grid->column('id', __('ID'))->sortable();
        $grid->column('agentNo', __('代理编号'));
        $grid->column('agentName', __('代理名称'));
        $grid->column('registerCount', __('注册会员'));
        $grid->column('firstDeposit', __('首存会员'));
        $grid->column('secondDeposit', __('二存会员'));
        $grid->column('thirdDeposit', __('三存会员'));
        $grid->column('depositAmount', __('总存款'))->display(function($val){
            return amountFilter($val);
        });
        $grid->column('withdrawAmount', __('提款'))->display(function($val){
            return amountFilter($val);
        });
        $grid->column('dwRate', __('存提比'));
        $grid->column('betValidAmount', __('有效投注'))->display(function($val){
            return amountFilter($val);
        });
        $grid->column('betValidPerson', __('投注人数'));
        $grid->column('payoutAmount', __('派彩额'))->display(function($val){
            return amountFilter($val);
        });
        $grid->column('promoteAmount', __('优惠'))->display(function($val){
            return amountFilter($val);
        });
        $grid->column('profitAmount', __('总盈利'))->display(function($val){
            return amountFilter($val);
        });
        $grid->column('created_at', __('创建时间'));

        $grid->disableActions();
//        $grid->actions(function ($actions) {
//            //关闭行操作 删除
//            $actions->disableView();
//            $actions->disableEdit();
//            $actions->disableDelete();
//        });

        //设置导出格式
        $exporter = Exporter::get($grid);
        $grid->exporter($exporter);

        $grid->model()->orderBy('created_at', 'desc');

        return $grid;
    }

    /**
     * 表单详情
     * @return Form
     */
    protected function form()
    {
        //TODO 检查权限
        $form = new Form(new CommissionDay);
        return $form;
    }

    /**
     * 部门详情
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
    }

}
