<?php


namespace App\Admin\Controllers\UserTools;

//use App\Models\Agent;
//use App\Models\Commission;
//use App\Models\CommissionRule;
use App\Admin\Controllers\PlugIn\FormBtn\BatchStatus;
use App\Models\CommissionStatistics;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
//use Encore\Admin\Show;
use Illuminate\Support\MessageBag;
use Zhusaidong\GridExporter\Exporter;
use Encore\Admin\Layout\Content;

class CommissionStatisticsController extends BaseController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '佣金提案';

    const _status = [
        -1 => '未通过',
        0 => '未审核',
        1 => '已审核',
    ];

    const status = [
        -1 => '<span class="label label-danger">未通过</span>',
        0 => '<span class="label label-default">未审核</span>',
        1 => '<span class="label label-success">已审核</span>',
    ];

    /**
     * 部门首页
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * 修改部门信息
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('修改')
            ->body($this->form($id)->edit($id));
    }

    public function show($id, Content $content)
    {
        return $content
            ->header($this->title)
            ->description('查看')
            ->body($this->form($id, 'show')->edit($id));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CommissionStatistics);

        $grid->batchActions(function ($batch) {
            $batch->disableDelete();
            $batch->add(new BatchStatus());
        });

        $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->like('agentName', __('代理名称'));
            $_statusOptions = ['' => '全部'] + self::_status;
            $filter->where(function ($query) {
                if ($this->input != '') {
                    $query->where('commissionStatus', $this->input);
                }
            }, __('佣金状态'), 'commissionStatus')->radio($_statusOptions);
            $filter->expand();
        });

        $grid->column('id', __('ID'))->sortable();
        $grid->column('agentName', __('代理名称'));
        $grid->column('commissionName', __('佣金方案名称'));
        $grid->column('lastSurplus', __('上期结余'))->display(function($val){
            return amountFilter($val);
        });
        $grid->column('commissionAmount', __('本期佣金'))->display(function($val){
            return amountFilter($val);
        });
        $grid->column('actualCommission', __('实际可领取佣金'))->display(function($val){
            return amountFilter($val);
        });
        $grid->column('validAmount', __('有效投注额'))->display(function($val){
            return amountFilter($val);
        });
        $grid->column('activeMember', __('活跃用户'))->display(function($val){
            return amountFilter($val);
        });
        $grid->column('payoutAmount', '总派彩')->display(function ($val) {
            return amountFilter($val);
        });
        $grid->column('status', __('方案状态'))->using(self::status);
        $grid->column('created_at', __('创建时间'));

        $grid->actions(function ($actions) {

            //关闭行操作 删除
            //$actions->disableView();
            if (!Admin::user()->isRole('administrator') || $actions->row->status == 1) {
                $actions->disableEdit();
                $actions->disableDelete();
            }

        });

        //设置导出格式
        $exporter = Exporter::get($grid);
        $grid->exporter($exporter);

        $grid->model()->orderBy('created_at', 'desc');

        return $grid;
    }

    /**
     * 表单详情
     * @param int $id
     * @return Form|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function form($id = 0, $show = '')
    {
        //TODO 检查权限
        $form = new Form(new CommissionStatistics);

        if ($show) {
            $form->column(1 / 2, function ($form) {
                $form->display('agentName', __('代理名称'));
                $form->display('commissionName', __('佣金方案名称'));
                $form->display('lastSurplus', __('上期结余'))->with(function ($value) {
                    return amountFilter($value);
                });
                $form->display('commissionAmount', __('本期佣金'))->with(function ($value) {
                    return amountFilter($value);
                });
                $form->display('actualCommission', __('实际可领佣金'))->with(function ($value) {
                    return amountFilter($value);
                })->help('实际可领佣金 = 本期佣金 - 上期结余 - 线下运营成本 - 银行卡手续费 - 其他成本. 审核后生成');
                $form->display('activeMember', __('活跃人数'));
                $form->display('validAmount', __('有效投注额'))->with(function ($value) {
                    return amountFilter($value);
                });
                $form->display('status', __('方案状态'))->options(self::_status)->default(0);
            });

            $form->column(1 / 2, function ($form) use ($id) {
                $form->display('profitAmount', '公司总盈利')->with(function ($profitAmount) {
                    return amountFilter($profitAmount);
                });
                $form->display('payoutAmount', '总派彩')->with(function ($payoutAmount) {
                    return amountFilter($payoutAmount);
                });
                $form->display('promoteAmout', '总优惠')->with(function ($promoteAmout) {
                    return amountFilter($promoteAmout);
                });
                $form->display('offlineCost', __('线下运营成本'));
                $form->display('bankRate', __('银行卡手续费'));
                $form->display('otherCost', __('其他成本'));
                if ($id > 0) {
                    $form->display('beginTime', '周期开始 - 周期结束')->with(function ($beginTime) {
                        return $beginTime . ' ~ ' . $this->endTime;
                    });
                } else {
                    $form->datetimeRange('beginTime', 'endTime', '周期开始 - 周期结束');
                }
                $form->textarea('remark', __('备注说明'));
            });

            $form->footer(function ($footer) {
                // 去掉`查看`checkbox
                $footer->disableViewCheck();

                // 去掉`继续编辑`checkbox
                $footer->disableEditingCheck();

                // 去掉`继续创建`checkbox
                $footer->disableCreatingCheck();
            });

            $form->tools(function (Form\Tools $tools){
                //去掉预览页面
                $tools->disableView();
                $tools->disableDelete();
            });

            $form->disableReset();
            $form->disableSubmit();
        } else {
            $form->column(1 / 2, function ($form) {
                $form->display('agentName', __('代理名称'));
                $form->display('commissionName', __('佣金方案名称'));
                $form->display('lastSurplus', __('上期结余'))->with(function ($value) {
                    return amountFilter($value);
                });
                $form->display('commissionAmount', __('本期佣金'))->with(function ($value) {
                    return amountFilter($value);
                });
                $form->display('actualCommission', __('实际可领佣金'))->with(function ($value) {
                    return amountFilter($value);
                })->help('实际可领佣金 = 本期佣金 - 上期结余 - 线下运营成本 - 银行卡手续费 - 其他成本. 审核后生成');
                $form->display('activeMember', __('活跃人数'));
                $form->display('validAmount', __('有效投注额'))->with(function ($value) {
                    return amountFilter($value);
                });
                $form->radio('status', __('方案状态'))->options(self::_status)->default(0);
            });

            $form->column(1 / 2, function ($form) use ($id) {
                $form->display('profitAmount', '公司总盈利')->with(function ($profitAmount) {
                    return amountFilter($profitAmount);
                });
                $form->display('payoutAmount', '总派彩')->with(function ($payoutAmount) {
                    return amountFilter($payoutAmount);
                });
                $form->display('promoteAmout', '总优惠')->with(function ($promoteAmout) {
                    return amountFilter($promoteAmout);
                });
                $form->text('offlineCost', __('线下运营成本'));
                $form->text('bankRate', __('银行卡手续费'));
                $form->text('otherCost', __('其他成本'));
                if ($id > 0) {
                    $form->display('beginTime', '周期开始 - 周期结束')->with(function ($beginTime) {
                        return $beginTime . ' ~ ' . $this->endTime;
                    });
                } else {
                    $form->datetimeRange('beginTime', 'endTime', '周期开始 - 周期结束');
                }
                $form->textarea('remark', __('备注说明'));
                $form->hidden('agentId', '代理ID');
                $form->hidden('rate', '佣金比例');
                $form->hidden('adminId', '管理员ID')->default(Admin::user()->id);
                $form->hidden('adminName', '管理员姓名')->default(Admin::user()->username);
                $form->hidden('auditTime', '审批时间')->default(date('Y-m-d H:i:s'));
                $form->hidden('auditIp', '审核IP')->default(request()->getClientIp());
            });

            $form->footer(function ($footer) {
                // 去掉`查看`checkbox
                $footer->disableViewCheck();

                // 去掉`继续编辑`checkbox
                $footer->disableEditingCheck();

                // 去掉`继续创建`checkbox
                $footer->disableCreatingCheck();
            });

            $form->tools(function (Form\Tools $tools){
                //去掉预览页面
                $tools->disableView();
                if (!Admin::user()->isRole('administrator')) {
                    $tools->disableDelete();
                }
            });

            $info = null;

            if ($id > 0) {
                $info = CommissionStatistics::where('id', $id)->first();
                if ($info['status'] == 1) {
                    header('Location:' . route('commissionStatistics.index'));
                    exit;
                }
            }

            $form->saving(function (Form $form) {
                if ($form->offlineCost < 0) {
                    $error = new MessageBag([
                        'title'   => '错误提示',
                        'message' => '线下运营成本必须为数字且不能小于0',
                    ]);
                    return back()->with(compact('error'));
                }

                if (floatval($form->bankRate) < 0) {
                    $error = new MessageBag([
                        'title'   => '错误提示',
                        'message' => '银行卡手续费必须为数字且不能小于0',
                    ]);
                    return back()->with(compact('error'));
                }

                if ($form->otherCost < 0) {
                    $error = new MessageBag([
                        'title'   => '错误提示',
                        'message' => '其他成本必须为数字且不能小于0',
                    ]);
                    return back()->with(compact('error'));
                }

                $form->offlineCost = intval($form->offlineCost);
                $form->bankRate = floatval($form->bankRate);
                $form->otherCost = intval($form->otherCost);

                //如果是审核通过
                if ($form->status == 1) {
                    $CommissionStatistics = CommissionStatistics::where('agentId', $form->agentId)
                        ->where('beginTime', '<=', $form->model()->beginTime)
                        ->where('status', 0)
                        ->first();
                    if ($CommissionStatistics) {
                        $error = new MessageBag([
                            'title'   => '错误提示',
                            'message' => '请先审核之前的佣金提案',
                        ]);
                        return back()->with(compact('error'));
                    }
//                    $agent = Agent::where('id', $form->agentId)->select('commissionId')->first();
//                    $commissionId = '';
//                    if ($agent) {
//                        $commissionId = $agent->commissionId;
//                    }
//                    if (!$commissionId) {
//                        $ctype = Commission::select('id')
//                            ->where('commissionStatus' , 1)
//                            ->where('isDefault', 1)
//                            ->first();
//                        if (!$ctype) {
//                            $error = new MessageBag([
//                                'title'   => '错误提示',
//                                'message' => '请设置佣金方案',
//                            ]);
//                            return back()->with(compact('error'));
//                        }
//                        $commissionId = $ctype->id;
//                    }
//
//                    //查询佣金规则
//                    $rules = CommissionRule::where('commissionId', $commissionId)->first();
//                    if (!$rules) {
//                        $error = new MessageBag([
//                            'title'   => '错误提示',
//                            'message' => '未找到佣金规则',
//                        ]);
//                        return back()->with(compact('error'));
//                    }

                    //公司总盈利 = 下线总派彩 - 下线总优惠
                    //$form->profitAmount = floatval($form->payoutAmount) - floatval($form->promoteAmout);

                    //本期佣金 = (公司总盈利-线下运营成本-银行卡手续费-其他成本) * 25%
                    $rate = $form->rate > 0 ? $form->rate : 0;

                    $form->commissionAmount = (floatval($form->model()->profitAmount) - floatval($form->offlineCost) - floatval($form->bankRate) - floatval($form->otherCost)) * $rate;

                    $form->actualCommission = (floatval($form->commissionAmount) - floatval($form->model()->lastSurplus));
                }
            });

            Admin::html("
            <script>
            function num(obj) {
                obj.value = obj.value.replace(/[^\-?\d.]/g, '');
//                obj.value = obj.value.replace(/[^\d.]/g,\"\"); //清除\"数字\"和\".\"以外的字符
//                obj.value = obj.value.replace(/^\./g,\"\"); //验证第一个字符是数字
//                obj.value = obj.value.replace(/\.{2,}/g,\".\"); //只保留第一个, 清除多余的
//                obj.value = obj.value.replace(\".\",\"$#$\").replace(/\./g,\"\").replace(\"$#$\",\".\");
                obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3'); //只能输入两个小数
            }
            </script>
            ");

            $script = "
            $('#offlineCost').unbind('keyup').on('keyup', function(){
                num(this)
            }).unbind('paste').on('paste', function(){
                num(this)
            });
    
            $('#bankRate').unbind('keyup').on('keyup', function(){
                num(this)
            }).unbind('paste').on('paste', function(){
                num(this)
            });
    
            $('#otherCost').unbind('keyup').on('keyup', function(){
                num(this)
            }).unbind('paste').on('paste', function(){
                num(this)
            });
    
            ";
            Admin::script($script);
        }


        return $form;
    }

}
