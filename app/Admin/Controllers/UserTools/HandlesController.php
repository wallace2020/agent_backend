<?php

namespace App\Admin\Controllers\UserTools;


use App\Models\CommissionStatistics;

class HandlesController extends BaseController
{

    public function _audit($id)
    {
        $status = request()->get('status', '');
        if (is_numeric($id)) {
            //进行批量审核
            if ($status == 1) {
                $clist = CommissionStatistics::where([
                    'id' => $id,
                    'status' => 0
                ])->first();
                $clist->commissionAmount = floatval($clist->profitAmount) * floatval($clist->rate);
                $clist->actualCommission = floatval($clist->commissionAmount) - floatval($clist->lastSurplus);
                $clist->status = 1;
                $update = $clist->save();
                if ($update) {
                    return 0;
                }
            } else {
                $cstatus = CommissionStatistics::where('id', $id)->update([
                    'status' => 0
                ]);
                if ($cstatus) {
                    return 0;
                }
            }
        }
        return -1;
    }

}
