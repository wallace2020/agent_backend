<?php

declare(strict_types=1);
namespace App\Admin\Controllers\UserTools;

use App\Admin\Controllers\PlugIn\FormBtn\IpRedis;
use Encore\Admin\Controllers\AdminController;
use Illuminate\Support\Facades\Redis;
use Zhusaidong\GridExporter\Exporter;
use Encore\Admin\Layout\Content;
use Encore\Admin\Facades\Admin;
use App\Models\IpWhilte;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class IpManagerController extends AdminController
{

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'IP白名单';

    /**
     * 部门首页
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * 创建部门信息
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('创建')
            ->body($this->form());
    }

    /**
     * 修改部门信息
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('修改')
            ->body($this->form()->edit($id));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ipRefresh() {
        Redis::set(config('admin.ipRedisKey'), null);
        $ipList = IpWhilte::setRedis();
        if ($ipList) {
            return response()->json(['code' => 0, 'message' => 'IP配置刷新成功', 'data' => '']);
        }
        return response()->json(['code' => -1, 'message' => 'IP配置刷新失败', 'data' => '']);
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new IpWhilte);

        $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->like('ip', __('IP地址'));
            $filter->like('remark', __('备注'));
            $filter->expand();
        });

        $grid->tools(function (Grid\Tools $tools) {
            $tools->append(new IpRedis);
        });

        $grid->column('id', __('ID'))->sortable();
        $grid->column('ip', __('IP地址'));
        $grid->column('remark', __('备注'));
        $grid->column('accessEndTime', __('访问结束时间'))->display(function($accessEndTime){
            if (empty($accessEndTime) || $accessEndTime == -1) {
                return '永久';
            }
            return date('Y-m-d H:i:s', strtotime($accessEndTime));
        });

        //设置导出格式
        $exporter = Exporter::get($grid);
        $grid->exporter($exporter);

        return $grid;
    }

    /**
     * 表单详情
     * @return Form
     */
    protected function form()
    {
        //TODO 检查权限
        $form = new Form(new IpWhilte);

        $form->ip('ip', 'IP地址')->required()->rules('required|ip')->setWidth('3');
        $form->text('remark', __('备注'))->setWidth('3');
        $form->date('accessEndTimes', __('允许访问时长'))->help('不填写为永久')->default('');
        $form->hidden('accessEndTime')->default(-1);
        $form->ignore('accessEndTimes');
        $form->saving(function (Form $form) {
            $accessEndTimes = request()->get('accessEndTimes');
            if ($accessEndTimes != '') {
                $form->accessEndTime = $accessEndTimes;
            }
        });
        $form->hidden('creator_id', __('发起人'))->default(Admin::user()->id);
        return $form;
    }

    /**
     * 部门详情
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        //TODO 检查权限
        $show = new Show(IpWhilte::findOrFail($id));
        return $show;
    }

}
