<?php


namespace App\Admin\Controllers\UserTools;

use App\Models\AgentLevel;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Zhusaidong\GridExporter\Exporter;
use Encore\Admin\Layout\Content;

class LevelController extends BaseController
{

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '代理等级';

    const _status = [
        0 => '未启用',
        1 => '启用',
    ];

    const status = [
        0 => '<span class="label label-default">未启用</span>',
        1 => '<span class="label label-success">启用</span>',
    ];

    /**
     * 部门首页
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * 修改部门信息
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('修改')
            ->body($this->form()->edit($id));
    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AgentLevel);

        $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->like('levelNo', __('层级编号'));
            $_statusOptions = ['' => '全部'] + self::_status;
            $filter->where(function ($query) {
                if ($this->input != '') {
                    $query->where('status', $this->input);
                }
            }, __('层级状态'), 'status')->radio($_statusOptions);
            $filter->expand();
        });

        $grid->column('id', __('ID'))->sortable();
        $grid->column('title', __('层级名称'));
        $grid->column('status', __('层级状态'))->using(self::status);
        $grid->column('remark', __('层级说明'));
        $grid->column('created_at', __('创建时间'));

        //设置导出格式
        $exporter = Exporter::get($grid);
        $grid->exporter($exporter);

        return $grid;
    }

    /**
     * 表单详情
     * @return Form
     */
    protected function form()
    {
        //TODO 检查权限
        $form = new Form(new AgentLevel);

        $form->text('title', __('层级名称'))->required()->rules('required')->setWidth(4);
        $form->switch('status', __('层级状态'))->default(1);
        $form->textarea('remark', __('层级说明'));
        $form->hidden('levelNo', __('层级编号'))->default(0);
        $form->hidden('adminId', __('操作人'))->default(Admin::user()->id);
        $form->hidden('adminName', __('操作人姓名'))->default(Admin::user()->username);

        return $form;
    }

    /**
     * 部门详情
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        //TODO 检查权限
        $show = new Show(AgentLevel::findOrFail($id));
        return $show;
    }

}
