<?php


namespace App\Admin\Controllers\UserTools;

use App\Models\Agent;
use App\Models\Translog;
use Encore\Admin\Grid;
use Zhusaidong\GridExporter\Exporter;
use Encore\Admin\Layout\Content;

class TranslogController extends BaseController
{

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '转账记录';

    /**
     * 记录首页
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Translog);

        $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            $filter->where(function($query){
                $query->where('toAgentName', 'like', "%{$this->input}%")
                    ->orWhere('toMemberName', 'like', "%{$this->input}%");
            }, __('用户名称'));
            $filter->between('created_at', __('创建时间'))->datetime();
            $filter->expand();
        });

        $grid->disableCreateButton();
        $grid->disableActions();

        $grid->column('id', __('ID'))->sortable();
        $grid->column('type', __('转账类型'))->display(function() {
            if ($this->toAgentId) {
                return '转账至代理';
            }
            return '转账至会员';
        });
        $grid->column('user', __('用户名称'))->display(function(){
            if ($this->toAgentId) {
                $agent = Agent::select('agentName')->where('id', $this->toAgentId)->first();
                return $agent ? $agent->agentName : '';
            }
            return '会员名称';
        });
        $grid->column('money', __('转账金额'))->display(function(){
            return amountFilter($this->money);
        });
        $grid->column('beforeAgentMoney', __('转账前代理余额'))->display(function (){
            return amountFilter($this->beforeAgentMoney);
        });
        $grid->column('afterAgentMoney', __('转账后代理余额'))->display(function (){
            return amountFilter($this->afterAgentMoney);
        });
        $grid->column('beforeMemberMoney', __('转账前用户金额'))->display(function (){
            return amountFilter($this->beforeMemberMoney);
        });
        $grid->column('afterMemberMoney', __('转账后用户金额'))->display(function (){
            return amountFilter($this->afterMemberMoney);
        });
        $grid->column('created_at', __('创建时间'));

        //设置导出格式
        $exporter = Exporter::get($grid);
        $grid->exporter($exporter);

        return $grid;
    }

}
