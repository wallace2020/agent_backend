<?php

namespace App\Admin\Controllers\UserTools;

use App\Models\Agent;
use Encore\Admin\Grid;
use Encore\Admin\Form;
use App\Models\Withdraw;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Zhusaidong\GridExporter\Exporter;
use Encore\Admin\Controllers\AdminController;

class WithdrawController extends AdminController
{

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '取款列表';

    const _type = [
        1 => '自发',
        2 => '后台',
    ];

    const type = [
        1 => '<span class="label label-default">自发</span>',
        2 => '<span class="label label-success">后台</span>',
    ];

    const _status = [
        -1 => '未通过',
         0 => '提交中',
         1 => '待审批',
         2 => '已通过',
    ];

    const status = [
        -1 => '<span class="label label-danger">未通过</span>',
         0 => '<span class="label label-default">提交中</span>',
         1 => '<span class="label label-warning">待审批</span>',
         2 => '<span class="label label-success">已通过</span>'
    ];

    /**
     * 取款首页
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * 取款信息
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        //TODO 检查权限
        return $content
            ->header($this->title)
            ->description('修改')
            ->body($this->form($id)->edit($id));
    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Withdraw);

        $grid->filter(function($filter){
            // 去掉默认的id过滤器
            $filter->disableIdFilter();

            $filter->like('bankName', __('银行卡名称'));
            $_statusOptions = ['' => '全部'] + self::_status;
            $filter->where(function ($query) {
                if ($this->input != '') {
                    $query->where('status', $this->input);
                }
            }, __('银行卡状态'), 'status')->radio($_statusOptions);

            $_typeOptions = ['' => '全部'] + self::_type;
            $filter->where(function ($query) {
                if ($this->input != '') {
                    $query->where('type', $this->input);
                }
            }, __('银行卡类型'), 'type')->radio($_typeOptions);
            $filter->expand();
        });

        $grid->column('id', __('ID'))->sortable();
        $grid->column('agentId', __('代理名称'))->display(function($agentId){
            $agent = Agent::select('agentName')->where('id', $agentId)->first();
            return $agent->agentName;
        });
        $grid->column('money', __('取款金额'))->display(function ($money){
            return amountFilter($money);
        });
        $grid->column('beforeAmount', __('取款前金额'))->display(function ($beforeAmount){
            return amountFilter($beforeAmount);
        });
        $grid->column('afterAmount', __('取款后金额'))->display(function ($afterAmount){
            return amountFilter($afterAmount);
        });
        $grid->column('status', __('银行卡状态'))->using(self::status);
        $grid->column('types', __('取款类型'))->using(self::type);
        $grid->column('bankName', __('取款银行卡'));
        $grid->column('bankAccount', __('银行账号'));
        $grid->column('bankUserName', __('开户姓名'));
        $grid->column('sources', __('取款来源'));
        $grid->column('ip', __('取款IP'));
        $grid->column('remark', __('备注'));
        $grid->column('created_at', __('创建时间'));
        $grid->column('auditTime', __('审核时间'));
        $grid->column('adminName', __('审核人'));

        $grid->actions(function ($actions) {
            //关闭行操作 删除
            $actions->disableView();
            if (!Admin::user()->isRole('administrator')) {
                $actions->disableEdit();
                $actions->disableDelete();
            }
        });

        if (!Admin::user()->isRole('administrator')) {
            $grid->disableRowSelector();
        }

        //设置导出格式
        $exporter = Exporter::get($grid);
        $grid->exporter($exporter);

        $grid->disableCreateButton();

        return $grid;
    }

    /**
     * 表单详情
     * @return Form
     */
    protected function form($id=0)
    {
        //TODO 检查权限
        $form = new Form(new Withdraw);

        $form->display('agentId', __('代理账号'))->with(function($agentId){
            $agent = Agent::select('agentName')->where('id', $agentId)->first();
            return $agent->agentName;
        });
        $form->display('money', __('取款金额'))->with(function($money){
            return amountFilter($money);
        });
        $form->display('beforeAmount', __('取款前金额'))->with(function($beforeAmount){
            return amountFilter($beforeAmount);
        });
        $form->display('afterAmount', __('取款后金额'))->with(function($afterAmount){
            return amountFilter($afterAmount);
        });
        $form->display('bankName', __('银行卡名称'));
        $form->display('bankCode', __('银行卡编号'));
        $form->display('bankAccount', __('银行卡账号'));
        $form->display('bankUserName', __('开户人姓名'));

        $auditTime = '';
        if ($id > 0) {
            $auditTime = Withdraw::select('auditTime')->where('id', $id)->first();
            $auditTime = $auditTime->auditTime;
        }

        $_status = self::status;
        if ($auditTime) {
            $form->display('status',  __('审批结果'))->with(function ($status) use ($_status) {
                return $_status[$status];
            });
            $form->textarea('remark', __('备注'))->readonly();
        } else {
            $form->select('status',  __('审批结果'))->options(self::_status)->required();
            $form->textarea('remark', __('备注'))->required();
        }
        $form->display('ip', __('取款IP'));
        $form->display('sources', __('取款来源'));
        $form->display('auditTime', __('审核时间'));
        $form->display('adminName', __('审核人'));
        $form->hidden('adminId');
        $form->hidden('adminName');
        $form->hidden('auditTime');
        $form->hidden('id');

        $form->tools(function (Form\Tools $tools){
            //去掉预览页面
            $tools->disableView();
            if (!Admin::user()->isRole('administrator')) {
                $tools->disableDelete();
            };
        });

        $form->saving(function(Form $form) {
            //减少余额
            $id = request()->input('id', '');
            $auditTime = request()->input('auditTime', '');
            if (empty($auditTime)) {
                if ($id > 0 && $form->status == 2) {
                    $agent = Agent::select('wallet')->where('id', $form->model()->agentId)->first();
                    if ($agent->wallet < $form->model()->money) {
                        return back()->with(admin_toastr('当前代理余额不足', 'error'));
                    }

                    $update = Agent::where('id', $form->model()->agentId)->decrement('wallet', $form->model()->money);
                    if (!$update) {
                        return back()->with(admin_toastr('审批失败', 'error'));
                    }
                }
                $form->adminId = Admin::user()->id;
                $form->adminName = Admin::user()->username;
                $form->auditTime = date('Y-m-d H:i:s');
            }
        });

        $form->saved(function(){
            //return redirect(route('withdraw.index'));
        });

        $form->footer(function ($footer) {
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();
            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();
        });

        return $form;
    }

    public function detail() {
        return redirect(route('withdraw.index'));
    }

}
