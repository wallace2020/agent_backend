<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {
    $router->get('/', 'HomeController@index')->name('admin.home');

    //佣金设置
    $router->resource('commission', 'UserTools\CommissionController');

    //佣金提案
    $router->resource('commissionStatistics', 'UserTools\CommissionStatisticsController');

    //IP管理
    $router->resource('ipManage', 'UserTools\IpManagerController');
    $router->group(['prefix' => 'ipManage'], function($router){
        $router->post('ipRefresh', 'UserTools\IpManagerController@ipRefresh')->name('ipManage.refresh');
    });

    //代理管理
    $router->resource('agent', 'UserTools\AgentManageController');

    //代理等级
    $router->resource('level', 'UserTools\LevelController');

    //代理设置
    $router->resource('config', 'UserTools\AgentConfigController');

    //佣金日报
    $router->resource('dayReport', 'UserTools\CommissionDayController');

    //银行卡管理
    $router->resource('bank', 'UserTools\BankController');

    //取款管理
    $router->resource('withdraw', 'UserTools\WithdrawController');

    //代理余额修改
    $router->resource('balance', 'UserTools\BalanceLogController');

    //转账记录
    $router->resource('translog', 'UserTools\TranslogController');

    //获取银行卡
    $router->get('getBankName', 'UserTools\BankController@getBankName');
    $router->get('getCommission', 'UserTools\CommissionController@getCommission');
    $router->get('getAgent', 'UserTools\AgentManageController@getAgent');
    $router->get('getParentOptions', 'UserTools\AgentManageController@getParentOptions');
});

//清除缓存
Route::get('clearCache', function(){
    $password = request()->get('pass', '');
    if ($password == config('admin.clear_pwd')) {
        \Illuminate\Support\Facades\Redis::set(config('admin.ipRedisKey'), null);
    }
});
