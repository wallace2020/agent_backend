<?php

namespace App\Http\Middleware;

use App\Models\IpWhilte;
use Closure;
use Illuminate\Support\Facades\Log;

class IpFilters
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //TODO 获取redis相关配置文件 没有则忽略
        $ipList = IpWhilte::setRedis();
        $ip = $request->ip();
        if (!in_array($ip, $ipList) || empty($ipList)) {
            Log::error("访问失败, 当前IP[{$ip}], IP列表[".json_encode($ipList)."]");
            exit('无权限访问，IP : ' . $ip);
        }

        return $next($request);
    }


}
