<?php


namespace App\Models;

use Baum\Node;

class Agent extends Node
{

    public $table = 'agent';

    protected $fillable = [
        'commissionId',
        'agentNo',
        'agentName',
        'password',
        'payword',
        'wallet',
        'level',
        'realNameValid',
        'phone',
        'phoneValid',
        'email',
        'emailValid',
        'registerSource',
        'registerIp',
        'inviteCode',
        'status',
        'apiToken',
        'parent_id',
        'left',
        'right',
        'depth',
        'lastLoginSource',
        'lastLoginIp',
        'lastLoginAt',
    ];

    public function info()
    {
        return $this->hasOne(AgentInfo::class, 'agentId', 'id');
    }

    public function bank()
    {
        return $this->hasMany(AgentBank::class, 'agentId', 'id');
    }
}
