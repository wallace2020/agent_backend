<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentBank extends Model
{

    public $table = 'agent_bank';

    protected $fillable = [
        'isMain',
        'isVisa',
        'bankCode',
        'bankName',
        'bankAccount',
        'bankUserName',
        'bankBranchName',
        'status',
        'province',
        'city',
        'area',
        'bankCount'
    ];

}
