<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentInfo extends Model
{

    public $table = 'agent_info';

    public $incrementing = false;//加上这句

    public $primaryKey = 'agentId';

    public $timestamps = false;

}
