<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentLevel extends Model
{

    public $table = 'agent_level';

    protected $fillable = [
        'levelNo',
        'title',
        'timeType',
        'minAmount',
        'maxAmount',
        'status',
        'remark',
        'adminId',
        'adminName'
    ];

}
