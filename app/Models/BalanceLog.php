<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalanceLog extends Model
{

    public $table = 'balance_log';

    protected $fillable = [
        'agentId',
        'agentName',
        'money',
        'type',
        'status',
        'creator_id',
        'creator_name',
        'approval_id',
        'approval_name',
        'auditTime',
        'auditIp',
        'remark'
    ];

    public function info()
    {
        return $this->hasOne(AgentInfo::class, 'agentId', 'id');
    }

    public function bank()
    {
        return $this->hasMany(AgentBank::class, 'agentId', 'id');
    }
}
