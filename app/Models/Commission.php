<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{

    public $table = 'agent_commission';


    public function level()
    {
        return $this->hasMany(CommissionRule::class, 'commissionId', 'id');
    }

    public function config()
    {
        return $this->hasMany(CommissionConfig::class, 'commissionId', 'id');
    }

}
