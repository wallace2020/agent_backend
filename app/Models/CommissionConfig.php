<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionConfig extends Model
{

    public $table = 'agent_commission_config';

    protected $fillable = [
        'commissionId',
        'fieldName',
        'type',
        'status',
        'required',
        'visible',
        'adminId',
        'adminName'
    ];

}
