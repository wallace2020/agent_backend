<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionRule extends Model
{

    public $table = 'agent_commission_rule';

    protected $fillable = [
        'commissionId',
        'commissionLevel',
        'minAmount',
        'maxAmount',
        'rate',
        'adminId',
        'adminName'
    ];
}
