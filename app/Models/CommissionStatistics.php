<?php
/**
 * Created by PhpStorm.
 * User: wangrenjie
 * Date: 2020/1/18
 * Time: 20:20
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionStatistics extends Model
{

    public $table = 'agent_commission_statistics';

    public $incrementing = false;//加上这句

    protected $fillable = [
        'agentId',
        'balance',
        'agentName',
        'proposalNo',
        'commissionTime',
        'commissionType',
        'commissionName',
        'lastSurplus',
        'commissionAmount',
        'status',
        'activeMember',
        'payoutAmount',
        'promoteAmout',
        'profitAmount',
        'actualCommission',
        'offlineCost',
        'bankRate',
        'otherCost',
        'remark',
        'rate',
        'beginTime',
        'endTime',
        'auditTime',
        'auditIp',
        'adminId',
        'adminName',
    ];

}
