<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

/**
 * Class IpWhilte
 * @package App\Models
 */
class IpWhilte extends Model
{
    public $table = 'ip_whilte';

    public static function getIps()
    {
        $ipList = [];
        //永久IP
        $list1 = self::select('ip')->where('accessEndTime', -1)->get();
        //临时IP
        $list2 = self::select('ip')->where('accessEndTime', '>=', date('Y-m-d H:i:s'))->get();
        if ($list1) {
            foreach ($list1 as $val) {
                $ipList[] = $val->ip;
            }
        }
        if ($list2) {
            foreach ($list2 as $val) {
                $ipList[] = $val->ip;
            }
        }
        return $ipList;
    }

    public static function setRedis()
    {
        $ipList = [];
        //Redis::set(config('admin.ipRedisKey'), null);
        try {

            if (Redis::exists(config('admin.ipRedisKey'))) {
                $ipList = Redis::get(config('admin.ipRedisKey'));
                if (!empty($ipList)) {
                    $ipList = json_decode($ipList);
                }
            }

            if (empty($ipList)) {
                $ipList = IpWhilte::getIps();
                Redis::set(config('admin.ipRedisKey'), json_encode($ipList));
            }

        } catch (\RedisException $e) {
            Log::error('缓存设置失败' . $e->getMessage());
        }

        return $ipList;
    }

}
