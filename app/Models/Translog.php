<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

/**
 * Class IpWhilte
 * @package App\Models
 */
class Translog extends Model
{
    public $table = 'agent_transfer_log';
}
