<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    public $table = 'agent_withdraw';

    /**
     * The attributes that are mass assignable.
     * 允许被写入的值
     * @var array
     */
    protected $fillable = [
        'id',
        'agentId',
        'money',
        'beforeAmount',
        'afterAmount',
        'remark',
        'status',
        'sources',
        'ip',
        'types',
        'adminId',
        'adminName',
        'auditTime'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * 查询后隐藏的值
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     * 查询后转换类型
     * @var array
     */
    protected $casts = [
    ];

}
