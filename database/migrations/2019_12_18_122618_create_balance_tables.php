<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateDeptTables
 * create by wallace at 2019 12 19
 * 代理余额修改记录
 */
class CreateBalanceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('agentId')->index()->comment('代理ID');
            $table->string('agentName', 50)->comment('代理姓名');
            $table->decimal('money', 24, 8)->default('0.00')->comment('金额');
            $table->tinyInteger('type')->default(1)->comment('添加方式 1增加 2减少');
            $table->tinyInteger('status')->default(0)->comment('审批结果 -1驳回 0待审批 1审批通过');
            $table->integer('creator_id')->comment('发起人');
            $table->string('creator_name', 30)->comment('发起人姓名');
            $table->integer('approval_id')->nullable()->comment('审批人');
            $table->string('approval_name', 30)->nullable()->comment('审批人姓名');
            $table->string('remark', 200)->nullable()->comment('备注');
            $table->timestamp('auditTime')->nullable()->comment('审批时间');
            $table->string('auditIp', 20)->nullable()->comment('审批IP');
            $table->timestamps();
            $table->comment = '代理余额修改申请';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(config('app.env') == 'local') {
            Illuminate\Support\Facades\Schema::dropIfExists('balance_log');
        }
    }
}
