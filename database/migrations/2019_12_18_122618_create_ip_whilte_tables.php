<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateDeptTables
 * create by wallace at 2019 12 19
 * 白名单表
 */
class CreateIpWhilteTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ip_whilte', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ip', 30)->unique()->comment('允许访问的IP');
            $table->string('remark', 200)->comment('访问备注');
            $table->string('accessEndTime', 100)->nullable('-1')->comment('允许访问时长 存时间戳，-1永久有效');
            $table->integer('creator_id')->comment('创建者ID');
            $table->timestamps();
            $table->comment = 'IP白名单';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(config('app.env') == 'local') {
            Illuminate\Support\Facades\Schema::dropIfExists('ip_whilte');
        }
    }
}
