INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `permission`, `created_at`, `updated_at`) VALUES
(1, 0, 1, '控制体', 'fa-bar-chart', '/', NULL, NULL, '2020-01-19 15:54:10'),
(2, 0, 2, '管理员', 'fa-tasks', NULL, NULL, NULL, '2020-01-19 15:54:19'),
(3, 2, 3, '用户', 'fa-users', 'auth/users', NULL, NULL, '2020-01-19 15:54:26'),
(4, 2, 4, '角色', 'fa-user', 'auth/roles', NULL, NULL, '2020-01-19 15:54:31'),
(5, 2, 5, '权限', 'fa-ban', 'auth/permissions', NULL, NULL, '2020-01-19 15:54:37'),
(6, 2, 6, '菜单', 'fa-bars', 'auth/menu', NULL, NULL, '2020-01-19 15:54:41'),
(7, 2, 7, '操作日志', 'fa-history', 'auth/logs', NULL, NULL, '2020-01-19 15:54:48'),
(8, 9, 13, '佣金设置', 'fa-area-chart', '/commission', NULL, '2020-01-19 15:55:29', '2020-02-09 13:44:37'),
(9, 0, 10, '佣金管理', 'fa-bars', NULL, NULL, '2020-01-19 15:55:53', '2020-02-09 13:44:37'),
(10, 9, 12, '佣金提案', 'fa-bars', '/commissionStatistics', NULL, '2020-01-19 15:56:26', '2020-02-09 13:44:37'),
(11, 2, 9, 'IP白名单', 'fa-bars', '/ipManage', NULL, '2020-01-19 15:56:44', '2020-02-09 13:44:37'),
(12, 0, 14, '代理管理', 'fa-history', NULL, NULL, '2020-01-19 15:57:06', '2020-02-09 13:44:37'),
(13, 12, 15, '代理列表', 'fa-bars', '/agent', NULL, '2020-01-19 15:58:04', '2020-02-09 13:44:37'),
(14, 12, 16, '代理等级', 'fa-bars', '/level', NULL, '2020-01-19 15:58:18', '2020-02-09 13:44:37'),
(15, 9, 11, '佣金日报表', 'fa-bars', '/dayReport', NULL, '2020-01-19 15:58:33', '2020-02-09 13:44:37'),
(16, 2, 8, '设置管理', 'fa-assistive-listening-systems', 'config', NULL, '2020-02-09 13:44:21', '2020-02-09 13:45:17');


INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1, 'All permission', '*', '', '*', NULL, NULL),
(2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL),
(3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL),
(4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL),
(5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL);

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2020-01-19 15:43:59', '2020-01-19 15:43:59');

INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL);

INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(1, 2, NULL, NULL);

INSERT INTO `admin_users` (`id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$gU65qIICfEQgPXDtHqhZh.4icoYjElOX2rLtq6cZz52m7ezdV3A3m', 'Administrator', NULL, 'xHIHpZcUBWc3LrIMlmsy5CZ6QICduKtjys2MASzOnEdL4nbyTAI58EG6FCuw', '2020-01-19 15:43:59', '2020-01-19 15:43:59'),
(2, 'fenix', '$2y$10$imX69.aGTkMRSbqJn6JP8u.hkA9oWwY6SdpNT97eQGH0.Ng4SPzVW', 'fenix', NULL, NULL, '2020-01-21 05:03:36', '2020-01-21 05:03:36');

INSERT INTO `admin_user_permissions` (`user_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(2, 1, NULL, NULL);

INSERT INTO `ip_whilte` (`id`, `ip`, `remark`, `accessEndTime`, `creator_id`, `created_at`, `updated_at`) VALUES
(1, '103.51.140.71', '103.51.140.71', '-1', 1, '2020-01-18 16:00:00', '2020-01-18 16:00:00'),
(2, '103.141.118.201', '103.141.118.201', '-1', 1, NULL, NULL),
(3, '223.119.58.36', '223.119.58.36', '-1', 1, '2020-01-21 04:04:09', '2020-01-21 04:04:09'),
(4, '103.17.28.108', '103.17.28.108', '-1', 1, '2020-01-21 06:42:19', '2020-01-21 06:42:19'),
(5, '154.204.1.196', '154.204.1.196', '-1', 1, '2020-01-31 16:00:00', '2020-01-31 16:00:00'),
(6, '112.209.179.70', '112.209.179.70', '-1', 1, '2020-02-01 16:00:00', '2020-02-01 16:00:00'),
(7, '203.160.166.142', '203.160.166.142', '-1', 1, '2020-02-05 16:00:00', '2020-02-05 16:00:00'),
(8, '110.54.193.92', '110.54.193.92', '-1', 1, '2020-02-08 16:00:00', '2020-02-08 16:00:00'),
(9, '112.209.196.92', '112.209.196.92', '-1', 1, '2020-02-08 16:00:00', '2020-02-08 16:00:00'),
(13, '112.209.108.155', '112.209.108.155', '-1', 1, '2020-02-18 16:00:00', '2020-02-18 16:00:00');
